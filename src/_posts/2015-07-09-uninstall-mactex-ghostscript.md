---
title: "Uninstalling MacTex and Ghostscript"
layout: post
---

[MacTex](https://www.tug.org/mactex/) is great, but also massive, and it includes a lot of features that I and most users will probably never need. That's why I decided to switch from MacTex to [BasicTex](https://www.tug.org/mactex/morepackages.html), which contains all of the essentials for compiling TeX documents and none of the stuff I'm never going to touch. I'd rather install something if and when I need it, and not have it sit around taking up space on my relatively small hard drive.

Fortunately, MacTex is [pretty easy to remove](https://www.tug.org/mactex/uninstalling.html). Less easy to remove (and much less useful to me) is Ghostscript, which is installed alongside MacTex. The method suggested on that page, to "Show Files" in the installer package and then delete those files one-by-one, is impossible to use, because the list is really long and the process of navigating to and deleting dozens of files by hand is extremely tedious.

So, after some [DuckDuckGo](https://duckduckgo.com/)-ing and StackOverflowing, I wrote a script that goes through this process automatically. It makes use of two special Mac programs: `pkgutil`, an omnibus utility for manipulating OS X installer packages, and `lsbom`, which parses the "Bom" files inside installer packages. Usage is simple: save the script anywhere on your computer, then run `bash uninstall-ghostscript.sh /path/to/mactex.pkg` where `/path/to/mactex.pkg` is the path and name of the MacTex installer package, something like like `~/Downloads/mactex-20150613.pkg`. The script (specifically `pkgutil --bom`) also writes some plain text files to the `/tmp` directory, so it will only work if you have write access there (the workaround is to run `pkgutil --extract`, which extracts the entire package to wherever you choose, and then to run `lsbom` on the file `local.pkg/Bom`; the `--bom` solution just seems cleaner to me).

Unfortunately, you will have to have kept a copy of the original MacTex installer package for this to work safely. Otherwise, you can download whatever version is available on the MacTex site, but there is no way for the script to tell what could have changed between versions. In case something goes wrong, the script has two failsafes: it prints the name of every file and directory it deletes, and it actually just moves everything to the user's Trash folder instead of deleting them outright.

The script is available [here](https://gist.github.com/gwerbin/dcba755b0484423e9e45).

As always, you should NEVER run a script from the Internet without reading it over first and making sure that you are comfortable with what it does. This script comes with ABSOLUTELY NO WARRANTY WHATSOEVER. By running it, you agree to take full responsibility for anything that happens to your computer as a result. _(Yes, my dad is a lawyer.)_ 
