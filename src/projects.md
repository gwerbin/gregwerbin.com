---
layout: page
published: true
title: Projects
long_title: Projects I've worked on
permalink: /projects/
order: 1
---

<ul class="post-list">
{% for project in site.data.links.projects %}
<li class="post-item-container">
  <h2 class="post-link-container"><a class="post-link" href="{{ project.url }}">{{ project.title }}</a></h2>
  <span class="post-meta">{{ project.subtitle }}</span>
</li>
{% endfor %}
</ul>
