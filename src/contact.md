---
layout: page
published: true
title: Contact
long_title: How to contact me
permalink: /contact/
order: 3
---

<!--sse-->

- [E-mail](mailto:{{ site.data.contacts.email }})
- [LinkedIn](https://www.linkedin.com/in/{{ site.data.contacts.linkedin }}).
- [GitHub](https://github.com/{{ site.data.contacts.github }})
- Stackexchange:
    - [StackOverflow](https://stackoverflow.com/users/{{ site.data.contacts.stackoverflow.id }}/{{ site.data.contacts.stackoverflow.username }})
    - [CrossValidated](https://crossvalidated.com/users/{{ site.data.contacts.crossvalidated.id }}/{{ site.data.contacts.crossvalidated.username }})

<!--
### Basic contacts
- [E-mail](mailto:{{ site.contacts.email }})
- ~~~PGP key~~~ _new key coming soon_
- [LinkedIn](https://www.linkedin.com/in/{{ site.contacts.linkedin }}).

### Research and software
- [GitHub](https://github.com/{{ site.contacts.github }})
- [GitLab](https://gitlab.com/u/{{ site.contacts.gitlab }})
- [ORCID](https://orcid.org/{{ site.contacts.orcid }})
- [Kaggle](https://www.kaggle.com/{{ site.contacts.kaggle }})
-->

<!--/sse-->
