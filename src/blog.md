---
layout: page
published: true
title: Blog
long_title: "Data science and miscellany"
permalink: /blog/
order: 2
---

<p class="feed-subscribe">Subscribe via <a href="{{ "/feed.xml" | relative_url }}">Atom</a></p>

<ul class="post-list">
{% for post in site.posts %}
<li class="post-item-container">
  <h2 class="post-link-container"><a class="post-link" href="{{ post.url | relative_url }}">{{ post.title }}</a></h2>
  <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
</li>
{% endfor %}
</ul>
