# Set JBUILD or JSERVE to specify additional command line options for "jekyll build" and "jekyll serve".

jekyll = bundle exec jekyll

# Build
Gemfile.lock: Gemfile
	bundle install
build: _site/public
_site:
	mkdir -p _site
_site/public: config.yml Gemfile.lock src _site
	$(jekyll) build --incremental --config $< $(JBUILD)
rebuild: config.yml # non-incremental version
	$(jekyll) build --config $< $(JBUILD)
serve: config.yml _site
	$(jekyll) serve --skip-initial-build --config $< $(JSERVE)

_site/.gitlab-ci.yml: .gitlab-ci.yml | _site
	cp .gitlab-ci.yml _site/.gitlab-ci.yml
_site/.git: | _site
	git init _site
	cd _site &&\
	    git remote add host git@gitlab.com:gwerbin/gwerbin.gitlab.io.git
deploy: _site/public _site/.gitlab-ci.yml _site/.git | _site
	cd _site &&\
	    git add public .gitlab-ci.yml &&\
	    git commit -m "Deploy" &&\
	    git push --force host master
# deploy: _site
# 	b2 sync --keepDays 7 --replaceNewer --compareVersions size $< b2://gregwerbin-dot-com/
git-push:
	git checkout working
	git push origin working
	git checkout master
	git merge --no-ff -X theirs working
	git push origin master
	git checkout working

clean:
	rm -rf _site/*
	rm -rf _site/.git
	rm -f _site/.gitignore

.PHONY: build rebuild clean serve deploy git-push
