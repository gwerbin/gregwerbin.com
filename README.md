[www.gregwerbin.com](http://www.gregwerbin.com)

My "professional" website. 

# Building

## Prerequisites

- [Ruby](https://ruby-lang.org) with [RubyGems](https://rubygems.org)
- [Bundler](https://bundler.io)
- [Pandoc](https://pandoc.org)
- [GNU Make](https://www.gnu.org/software/make/)

## Instructions

First-time setup:

```shell
bundle install
make rebuild
```

Serve locally:

```shell
make serve
```

Incremental build:

```shell
make build
```

Clean and rebuild:

```shell
make clean
make rebuild
```
